// Función que suma los números ingresados
function sumar() {
	// Se obtiene el primer número ingresado
	let num1 = prompt("Ingrese un número: ");
	
	// Se obtiene el segundo número ingresado
	let num2 = prompt("Ingrese un número: ");
	
	// para obtener el valor numérico se utiliza parseInt
	let suma = parseInt(num1) + parseInt(num2);
	
	// Se muestra el resultado de la suma
	alert("La suma de los numeros ingresados es: " + suma);
}
