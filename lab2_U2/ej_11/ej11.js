// Arreglo con las imagenes 
var imagenes = new Array("../imagenes/1", "../imagenes/2", "../imagenes/3",
						 "../imagenes/4", "../imagenes/5")

// Contador
var cont = 0;

// Función para imagen siguiente
function avanza() {
	// Evita que se salga del arreglo de tamaño 4
	if (cont < 4) {
		// Actualiza el contador
		cont++;
        // Cambia la imagen, actualizando el arreglo con el contador
        document.getElementById("img").src = imagenes[cont];
    }	
}

function retrocede() {
	// Primera posición del arreglo
	if (cont > 0) {
		// Actualiza el contador
		cont--;
        // Cambia la imagen, actualizando el arreglo con el contador
        document.getElementById("img").src = imagenes[cont];
    }	
}
