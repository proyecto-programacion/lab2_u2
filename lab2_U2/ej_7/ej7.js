// Función que indica si esta una vocal o no en una frase
function VocalesFrase() {
	// Se obtiene la frase
    let frase = prompt("Ingrese una frase: ");
    
    // Se tranforma la frase a letras minúsculas
    minusculas = frase.toLowerCase();
    
    // Se recorre el largo de la frase
    for(let i=0; i<minusculas.length; i++){
		// Se evalua la frase con substrings de cada vocal y si se encuentra lo mustra
		if(minusculas.substr(i,1) == "a" || minusculas.substr(i,1) == "e" ||
			minusculas.substr(i,1) == "i" || minusculas.substr(i,1) == "o" ||
			minusculas.substr(i,1) == "u") {
				alert ("En la palabra se encuentra la: " + minusculas.substr(i,1));
		}
	}
}
