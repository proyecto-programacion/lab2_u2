// Función que simula el lanzamiento al azar de un dado
function lanzar() {
	// Asigna una variable random entre los números 1 y 6
	let lanzamiento = Math.round((Math.random() * 6)+1);
	console.log(lanzamiento);
	
	// Posibles casos para cada número random con switch
    switch (lanzamiento) {
        case 1:
            // Dado con cara 1
            document.getElementById('dado').src = "../dados/dado1.png";
            // Se termina el ciclo
            break;
        case 2:
            // Dado con cara 2
            document.getElementById('dado').src = "../dados/dado2.png";
            // Se termina el ciclo
            break;
        case 3:
            // Dado con cara 3
            document.getElementById('dado').src = "../dados/dado3.png";
            // Se termina el ciclo
            break;
        case 4:
            // Dado con cara 4
            document.getElementById('dado').src = "../dados/dado4.png";
            // Se termina el ciclo
            break;
        case 5:
            // Dado con cara 5
            document.getElementById('dado').src = "../dados/dado5.png";
            // Se termina el ciclo
            break;
        default:
            // Dado con cara 6
            document.getElementById('dado').src = "../dados/dado6.png";
            // Se termina el ciclo
            break;
    }

}
