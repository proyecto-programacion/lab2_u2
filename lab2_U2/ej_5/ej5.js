// Función que determina si un número es divisible por 2
function DivisiblePorDos() {
    // Obtenemos un número
    let num1 = prompt("Ingrese un número: ");
    
    // Condición para ver si es par (divisible por 2)
    if ((num1%2) == 0){
        alert(num1 + " es divisible por 2");
    }
    else {
		alert("El número " + num1 + " no es divisible por 2")}
}

