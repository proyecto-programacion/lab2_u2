// Función que cuenta el caracter "a" en una frase
function cuentaAs() {
    // Se obtiene la frase
    let frase = prompt("Ingrese una frase: ");
    
    // Contador en 0
    let cont = 0;
    
    // Se recorre el largo de la frase ingresada
    for (let i=0; i<frase.length; i++){
		// toLowerCase transforma todo a minúscula
		if(frase[i].toLowerCase() == "a") {
			// Si encuentra una "a" el contador aumenta
			cont++;
		}
	}
	// Se muestra la cantidad de "a" en la frase
	alert("En la frase: " + frase + " hay un total de " + cont + " a");
}

