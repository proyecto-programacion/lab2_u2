// Función para transformar dólares a pesos chilenos (CLP)
function cambiarDolar() {
	// Asigna una variable al valor ingresado por el usuario a través
	// del método .getElementById
	var cambioDolar = document.getElementById("dolar").value;
	let pesos = cambioDolar * 852.51;
	document.getElementById("clp").value =  pesos;	
}

// Función para transformar pesos chilenos (CLP) a dólares
// Mantiene misma dinámica que la función anterior
function cambiarPeso() {
	var cambioPeso = document.getElementById("peso").value;
	let USD = cambioPeso / 852.51;
	document.getElementById("usd").value =  USD;
}


